package operation;

/**
 * @author anton.litvinov
 */
public class Update extends AbstractOperation {
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.UPDATE;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
