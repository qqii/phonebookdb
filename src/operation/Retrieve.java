package operation;

/**
 * @author anton.litvinov
 */
public class Retrieve extends AbstractOperation{
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.RETRIEVE;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
