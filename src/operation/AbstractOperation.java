package operation;

/**
 * @author anton.litvinov
 */
public abstract class AbstractOperation implements Operation {
    public abstract OperationCode getExecutedOperationCode();
    public abstract String executeOperation();
}
