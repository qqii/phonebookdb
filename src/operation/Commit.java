package operation;

/**
 * @author anton.litvinov
 */
public class Commit extends AbstractOperation {
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.COMMIT;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
