package operation;

/**
 * @author anton.litvinov
 */
public class Delete extends AbstractOperation {
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.DELETE;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
