package operation;

/**
 * @author anton.litvinov
 */
public class Create extends AbstractOperation {
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.CREATE;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
