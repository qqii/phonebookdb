package operation;

/**
 * @author anton.litvinov
 */
public class Snapshot extends AbstractOperation {
    @Override
    public OperationCode getExecutedOperationCode() {
        return OperationCode.SNAPSHOT;
    }

    @Override
    public String executeOperation() {
        return null;
    }
}
