package operation;

/**
 * @author anton.litvinov
 */
public enum OperationCode {
    CREATE,
    RETRIEVE,
    UPDATE,
    DELETE,
    COMMIT,
    SNAPSHOT
}
