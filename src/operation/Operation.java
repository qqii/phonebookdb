package operation;

/**
 * @author anton.litvinov
 */
public interface Operation {
    public String executeOperation();
}
