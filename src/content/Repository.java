package content;

import model.PhoneRecord;

import java.util.*;

/**
 * @author anton.litvinov
 */
public class Repository {
    public static final Repository INSTANCE = new Repository();
    private Set<PhoneRecord> phoneBook = new HashSet<PhoneRecord>();

    public boolean add(PhoneRecord record){
        if(phoneBook.contains(record)){
            return false;
        }else{
            return phoneBook.add(record);
        }
    }

    public List<PhoneRecord> get(String surname){
        ArrayList<PhoneRecord> records = new ArrayList<PhoneRecord>();
        for(PhoneRecord record : phoneBook){
            if(record.getSurname().toUpperCase().equals(surname.toUpperCase())){
                records.add(record);
            }
        }
        return records;
    }

    public boolean update (String surname, PhoneRecord record){
        for(PhoneRecord phoneRecord: phoneBook){
            if(phoneRecord.getSurname().toUpperCase().equals(surname.toUpperCase())){
                phoneBook.remove(phoneRecord);
                phoneBook.add(record);
                return true;
            }
        }
        return false;
    }

    public boolean delete(String surname){
        for(PhoneRecord phoneRecord: phoneBook){
            if(phoneRecord.getSurname().toUpperCase().equals(surname.toUpperCase())){
                phoneBook.remove(phoneRecord);
                return true;
            }
        }
        return false;
    }
}
