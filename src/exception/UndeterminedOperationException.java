package exception;

/**
 * @author anton.litvinov
 */
public class UndeterminedOperationException extends Exception {
    public UndeterminedOperationException() {
        super();
    }

    public UndeterminedOperationException(String message) {
        super(message);
    }

    public UndeterminedOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UndeterminedOperationException(Throwable cause) {
        super(cause);
    }

    protected UndeterminedOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
