package handler;

import exception.UndeterminedOperationException;
import operation.*;

import java.lang.reflect.UndeclaredThrowableException;

/**
 * @author anton.litvinov
 */
public class OperationHandler {

    public static Operation determiteOperation(String command)throws UndeterminedOperationException{
        OperationCode operationCode = getCommandFromString(command);
        switch (operationCode){
            case CREATE:
                return new Create();
            case DELETE:
                return new Delete();
            case RETRIEVE:
                return new Retrieve();
            case UPDATE:
                return new Update();
            case COMMIT:
                return new Commit();
            case SNAPSHOT:
                return new Snapshot();
            default:
                return null;
        }
    }

    private static OperationCode getCommandFromString(String command) throws UndeterminedOperationException{
        for(OperationCode operationCode : OperationCode.values()){
            if(command.toUpperCase().equals(operationCode.toString())){
                return operationCode;
            }
        }
        throw new UndeterminedOperationException(command);
    }
}
